#ToriOS Manual V 2

This is the page for the new ToriOS Manual.  You can find our [current version here](https://github.com/zleap/Torios-2017). 

![ToriOS Logo](https://github.com/zleap/ToriosManualv2/blob/master/screens/FinalLogo.png)

**ABOUT THIS MANUAL**

Lanugage : English
Created in : [LaTeX](https://www.latex-project.org/)
Using : [Overleaf](https://www.overleaf.com/)

**CONTRIBUTING**

In order to contribute to this manual you need.
1. Access to a computer (PC or Laptop)
2. Access to a 2nd computer you can use to Test ToriOS
3. Install a virtual machine (qemu, for example)
4. Access to the Internet
5. Access to e-mail

Firstly please sign up to 
[Launchpad](https://launchpad.net/)
and Join
[LaunchPad Torios Team](https://launchpad.net/~torios)
From here you can join the mailing list and get updates on the ToriOS project.  

As well as the
[ToriOS Develpers Team](https://launchpad.net/~torios-dev)

Again there is a mailing list where you can discuss development of ToriOS.

You will be familiar with [Github](https://github.com/), or at least be able to :
Git clone the project and see what the progress is so far. 
You may want to also read up on [Bazaar](https://bazaar.canonical.com/en/) which is similar to github. 

If you need help please ask on the mailing list(s)

Projects based on GNU Linux generally pull together other projects to create a distribution.
ToriOS is now based on Debian.
ToriOS uses

 - [OBI One button Installer](https://help.ubuntu.com/community/OBI) 
 - [JWM Window manager](http://joewing.net/projects/jwm/)

Contributing to the documentation also helps the ToriOS project documentation, and also keeps all these manuals small and specific.  

What I would like help with, in terms of the ToriOS docs is content.  Either that or we create:

 - A separate install manual 
 - A separate testing manual

This will help to keep documents speciffic.

If you would to learn LaTeX.  You have several options.
  
https://github.com/zleap/zleap-wiki/wiki/LaTeX

Has a list of a few websites that teach LaTeX. If you don't want to install LaTeX and TeXLive, then Overleaf is probably a good start.  

If you are at college then ask if LaTeX is installed on the academic network. 

https://www.texstudio.org/  -  is cross platform

You can also join #latex on freenode (IRC)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjExMzU3ODQwMSwtNzY0NTQzMzc1XX0=
-->